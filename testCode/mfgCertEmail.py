#checks for cert if too low sends email

import smtplib
import email
import config
import time
num_certs = 249
smtpServer = smtplib.SMTP('smtp.gmail.com', 587)

def cert_email(subject, msg):
  try:
    smtpServer.ehlo()
    smtpServer.starttls()
    smtpServer.login(config.SENDER_ADDRESS, config.PASSWORD)
    message = 'Subject: {}\n\n{}'.format(subject, msg)
    smtpServer.sendmail(config.SENDER_ADDRESS, config.RECIEVER_ADDRESS, message)
    smtpServer.quit
    print("Success! Email sent")
  except:
    print("Failed to send email")

subject = "URGENT!!: MFG CERTIFICATIONS LOW:  " + str(num_certs)
msg = "Please add more MFG certifications, they are at: " + str(num_certs)

def anti_spam_alert():
  antiSpamCount = 0
  if antiSpamCount < 19:
    antiSpamCount += 1 
    print("Not spamming, email was sent this many monitors ago: " + str(antiSpamCount))
    return
  elif antiSpamCount >= 20:
    cert_email(subject, msg)
    antiSpamCount = 0
    print("Sent low cert warning! Reset Anti Spam...")
  elif num_certs > 251:
      return
  else:
    print("Did not send alert.")

anti_spam_alert()
